import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';

import { Observable } from 'rxjs/Observable';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

export interface TodoItem { title: null; }


@Component({
    selector: 'todo-container',
    templateUrl: 'todo.html'
})



export class TodoPage {
	   newTodo;
     todos: Observable<any[]>;
     todoQuery;
     todo = {
        title: '',
        description: ''
      };
	constructor(public navCtrl: NavController, public db: AngularFirestore) {
          this.newTodo = <TodoItem>{};
          this.todoQuery = db.collection('todos');
          this.todos = this.todoQuery.valueChanges();

          
         
     }

     submitTodo() {
         //this.todoQuery.add(this.newTodo);
         
         console.log(this.newTodo.title);
         console.log(123);
         this.newTodo =  {} ;
     }

     /*logForm(form) {
       console.log(form.value)
     }*/

}