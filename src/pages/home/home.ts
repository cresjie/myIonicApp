import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { GooglePlus } from '@ionic-native/google-plus';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	user;

  constructor(public navCtrl: NavController, public afAuth: AngularFireAuth, public gPlus: GooglePlus) {
  	//this.user = this.afAuth.auth.currentUser;
  	var self = this;
  	var a = this.afAuth.auth.onAuthStateChanged(function(user){
  		self.user = user;
      console.log(self.user)
  	});
  }

  loginGoogle() {
  	/*this.afAuth.auth.signInWithPopup( new firebase.auth.GoogleAuthProvider() )
  		.then(function(response){
  			window.location.reload();
  		});*/
  	this.gPlus.login({
      'webClientId': '254114236047-24oqrer035mjpigue3pcej9a6iu3p8ur.apps.googleusercontent.com',
      'offline': true
    }).then(function(response){
      console.log(response)
    });
  }

  logout() {
    this.afAuth.auth.signOut();
    window.location.reload();
  }

}
